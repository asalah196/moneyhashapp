# MoneyHashApp

- This is an aggregated package for MoneyHashTask package and KingFisher.
- It provides you with a component that's ready to be displayed per your need.
- It's been done using purely SwiftUI

## Known Issues
- ~~User's text input not binding with the view model~~
- ~~A minor UI bug (user interaction responsiveness)~~

## TODO
- ~~Fix the bugs~~
- Create a logger for the errors
- Honor all values returned for the SDK for a fully server-driven UI
- Smooth the animations
- Force the user to input numbers only when a number is expected (e.g. Age)
