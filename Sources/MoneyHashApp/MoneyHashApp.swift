import SwiftUI
import UIKit

public struct MoneyHashApp {
    public private(set) var text = "Hello, World!"

    /// Returns a ready to be dispalyed ViewController to be displayed in UIKit components
    static public func viewController() -> UIViewController {
        return UIHostingController(rootView: PaymentMethodView())
    }

    /// Returns a ready to be dispalyed SwiftUI View to be displayed in SwiftUI components
    static public func swiftUIView() -> AnyView {
        return AnyView(PaymentMethodView())
    }
}
