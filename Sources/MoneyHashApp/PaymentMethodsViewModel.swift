//
//  PaymentMethodsViewModel.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/28/23.
//

import SwiftUI
import MoneyHashTask

final class PaymentMethodsViewModel: NSObject, ObservableObject {
    @Published var components: [any UIComponent] = []
    let paymentActionsManager = PaymentActionsManger()
    var selectedMethod: String?

    override init() {
        super.init()
        paymentActionsManager.handleAction(nextAction: .Initial()) { [weak self] content, error in
            Configuration.setThemeColor(named: content?.brandSettings?.brandPrimaryColor)
            Configuration.setAccentColor(named: content?.brandSettings?.brandAccentColor)
            self?.components = FormBuilder().buildForm(from: content?.fields ?? [])
        }
    }

    @discardableResult
    func userDidPress(component: some UIComponent) -> String?{
        if let methodComponent = component as? MethodComponent {
            guard selectedMethod != methodComponent.uiModel.name else {
                return nil
            }
            selectedMethod = methodComponent.uiModel.name
            updateComponents(with: methodComponent.uiModel.name)
        } else if let buttonComponent = component as? ButtonComponent {
             return buttonPressed(with: buttonComponent.uiModel.action)
        }
        return nil
    }

    func updateComponents(with selectedMethod: String) {
        paymentActionsManager.handleAction(nextAction: .UpdateMethod(methodName: selectedMethod) ) { [weak self] content, error in
            self?.components = FormBuilder().buildForm(from: content?.fields ?? [])
        }
    }

    func buttonPressed(with action: ExpectedAction) -> String? {
        switch action {
        case .submit:
            return validateUserData()
        default:
            fatalError("Unexpected action type: \(action)")
        }

    }

    private func validateUserData() -> String? {

        var form: [String: Any] = [:]
        var errorMessage: String?
        components.forEach { component in
            if let textInputComponent = component as? TextInputComponent {
                let text = textInputComponent.text
                let validation = textInputComponent.uiModel.validation
                let validationResult = Validator().validate(validation: validation, content: text)
                guard validationResult.isValid else {
                    errorMessage = validationResult.errorMessage
                    return
                }
                form[textInputComponent.uiModel.name] = text
            }
        }
        return errorMessage
    }

    private func submitPayment(with form: [String: Any]) {
        guard let selectedMethod = selectedMethod else {
            fatalError("Submitting a form with payment method")
        }

        paymentActionsManager.handleAction(nextAction: .SubmitPaymentForm(form: form, selectedMethod: selectedMethod)) { content, error in
            self.components = FormBuilder().buildForm(from: content?.fields ?? [])
        }
    }

}
