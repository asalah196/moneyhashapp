//
//  Configuration.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/30/23.
//

import Foundation
import SwiftUI

struct Configuration {
    static var themeColor: Color = Color.blue
    static var accentColor: Color = Color.white
    static func setThemeColor(named color: String?) {
        if let color = color {
            themeColor = Color(hex: color)
        }
    }
    static func setAccentColor(named color: String?) {
        if let color = color {
            accentColor = Color(hex: color)
        }
    }
}

fileprivate extension Color {
    init(hex: String) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int: UInt64 = 0
        Scanner(string: hex).scanHexInt64(&int)
        let r = int >> 16
        let g = int >> 8 & 0xFF
        let b = int & 0xFF


        self.init(
            .sRGB,
            red: Double(r) / 255,
            green: Double(g) / 255,
            blue:  Double(b) / 255,
            opacity: 255
        )
    }
}
