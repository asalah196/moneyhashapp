//
//  SpacerParser.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/29/23.
//

import Foundation
import MoneyHashTask
import SwiftUI

struct SpacerParser: Parser {
    static func parse(_ field: Field) -> some UIComponent {
        guard let spacerField = field as? Field.Spacer else {
            fatalError("Unexpectedly found \(field.self) instead of Field.Spacer")
        }

        let uiModel = SpacerModel(height: spacerField.height.cgfloat,
                                    leading: spacerField.start.cgfloat,
                                    trailing: spacerField.end.cgfloat,
                                    bottom: spacerField.bottom.cgfloat,
                                    top: spacerField.top.cgfloat)
        return SpacerComponent(uiModel: uiModel)
    }
}

extension Int32 {
    var cgfloat: CGFloat {
        return CGFloat(self)
    }
}
