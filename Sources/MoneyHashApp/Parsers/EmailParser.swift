//
//  EmailParser.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/29/23.
//

import Foundation
import MoneyHashTask

struct EmailParser: Parser {
    static func parse(_ field: Field) -> some UIComponent {
        guard let emailField = field as? Field.Email else {
            fatalError("Unexpectedly found \(field.self) instead of Field.Email")
        }
        let uiModel = TextInputModel(placeHolder: emailField.helpText ?? "", label: emailField.label, validation: emailField.validation, name: emailField.name)
        return TextInputComponent(uiModel: uiModel, keyboardType: .emailAddress)
    }
}
