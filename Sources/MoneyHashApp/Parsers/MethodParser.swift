//
//  MethodParser.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/30/23.
//

import Foundation
import MoneyHashTask

struct MethodParser: Parser {
    static func parse(_ field: Field) -> some UIComponent {
        guard let methodField = field as? Field.Method else {
            fatalError("Unexpectedly found \(field.self) instead of Field.Spacer")
        }

        let uiModel = MethodModel(name: methodField.name,
                                    label: methodField.label,
                                    icons: methodField.icons,
                                    isSelected: methodField.isSelected)
        return MethodComponent(uiModel: uiModel)
    }
}
