//
//  LabelParser.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/29/23.
//

import Foundation
import MoneyHashTask
import SwiftUI

struct LabelParser: Parser {
    static func parse(_ field: Field) -> some UIComponent {
        guard let labelField = field as? Field.Label else {
            fatalError("Unexpectedly found \(field.self) instead of Field.Label")
        }

        let alignment = bindAlignment(form: labelField.gravity)

        let uiModel = LabelModel(alignment: alignment, text: labelField.title)
        return LabelComponent(uiModel: uiModel)
    }

    private static func bindAlignment(form gravity: ViewGravity) -> Alignment {
        switch gravity {
        case .end:
            return .trailing
        case .start:
            return .leading
        case .center:
            return .center
        default:
            return .center
        }
    }
}
