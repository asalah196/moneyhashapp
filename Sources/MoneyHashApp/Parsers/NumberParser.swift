//
//  NumberParser.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/29/23.
//

import Foundation
import MoneyHashTask

struct NumberParser: Parser {
    static func parse(_ field: Field) -> some UIComponent {
        guard let numberField = field as? Field.Number else {
            fatalError("Unexpectedly found \(field.self) instead of Field.Number")
        }
        let uiModel = TextInputModel(placeHolder: numberField.helpText ?? "", label: numberField.label, validation: numberField.validation, name: numberField.name)
        return TextInputComponent(uiModel: uiModel, keyboardType: .numberPad)
    }
}
