//
//  TextInputParser.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/29/23.
//

import Foundation
import MoneyHashTask

struct TextInputParser: Parser {
    static func parse(_ field: Field) -> some UIComponent {
        guard let textInputField = field as? Field.TextInput else {
            fatalError("Unexpectedly found \(field.self) instead of Field.TextInput")
        }
        let uiModel = TextInputModel(placeHolder: textInputField.helpText ?? "", label: textInputField.label, validation: textInputField.validation, name: textInputField.name)
        return TextInputComponent(uiModel: uiModel, keyboardType: .default)
    }
}
