//
//  ButtonParser.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/29/23.
//

import Foundation
import MoneyHashTask

struct ButtonParser: Parser {
    static func parse(_ field: Field) -> some UIComponent {
        guard let buttonField = field as? Field.Button else {
            fatalError("Unexpectedly found \(field.self) instead of Field.Button")
        }
        let uiModel = ButtonModel(label: buttonField.label, action: buttonField.action)
        return ButtonComponent(uiModel: uiModel)
    }
}
