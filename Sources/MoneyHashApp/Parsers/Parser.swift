//
//  Parser.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/29/23.
//

import Foundation
import MoneyHashTask
protocol Parser {
    associatedtype T: UIComponent
    static func parse(_ field: Field) -> T
}
