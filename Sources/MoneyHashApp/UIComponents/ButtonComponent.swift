//
//  ButtonComponent.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/28/23.
//

import Foundation
import SwiftUI
import MoneyHashTask

struct ButtonComponent: UIComponent {
    var uniqueId: String = UUID().uuidString
    let uiModel: ButtonModel

    func render(with tapAction: (() -> ())?) -> AnyView {
        ButtonView(uiModel: uiModel, tapAction: tapAction).toAny()
    }
}

struct ButtonModel {
    let label: String
    let action: ExpectedAction
}

struct ButtonView: View {
    let uiModel: ButtonModel
    var tapAction: (() -> ())?
    var body: some View {
        Button {
            tapAction?()
        } label : {
            Text(uiModel.label)
                .foregroundColor(Configuration.accentColor)
        }
        .frame(minWidth: 150, minHeight: 50)
        .contentShape(Rectangle())
        .background(Configuration.themeColor)
        .cornerRadius(50)
    }
}
