//
//  UIBuilder.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/28/23.
//

import Foundation
import SwiftUI

protocol UIComponent {
    var uniqueId: String  { get }
    func render(with tapAction: (() -> ())?) -> AnyView
}

extension View {
    func toAny() -> AnyView {
        return AnyView(self)
    }
}
