//
//  SpacerComponent.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/30/23.
//

import Foundation
import SwiftUI

struct SpacerComponent: UIComponent {

    var uiModel: SpacerModel
    var uniqueId: String = UUID().uuidString
    func render(with tapAction: (() -> ())?) -> AnyView {
        SpacerView(uiModel: uiModel).toAny()
    }
}


struct SpacerModel {
    var height: CGFloat
    var leading: CGFloat
    var trailing: CGFloat
    var bottom: CGFloat
    var top: CGFloat
}

struct SpacerView: View {
    var uiModel: SpacerModel
    var body: some View {
        SwiftUI.Spacer()
            .padding(.leading, uiModel.leading)
            .padding(.trailing, uiModel.trailing)
            .padding(.top, uiModel.top)
            .padding(.bottom, uiModel.bottom)
            .padding(.vertical, uiModel.height)
    }
}
