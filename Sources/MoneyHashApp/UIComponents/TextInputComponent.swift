//
//  TextInputComponent.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/29/23.
//

import Foundation
import SwiftUI
import MoneyHashTask
import Combine

struct TextInputComponent: UIComponent {

    var uniqueId = UUID().uuidString
    @ObservedObject var uiModel: TextInputModel
    var keyboardType: UIKeyboardType?

    var text: String {
        return uiModel.text
    }

    func render(with tapAction: (() -> ())?) -> AnyView {
        TextInputView(uiModel: uiModel, keyboardType: keyboardType ?? .default).toAny()
    }
}

class TextInputModel: ObservableObject {
    var placeHolder: String
    var label: String
    var validation: Validation
    var name: String
    var text: String = ""

    init(placeHolder: String, label: String, validation: Validation, name: String) {
        self.placeHolder = placeHolder
        self.label = label
        self.validation = validation
        self.name = name
    }
}

struct TextInputView: View {
    @StateObject var uiModel: TextInputModel
    var keyboardType: UIKeyboardType
    var body: some View {
        HStack {
            Text(uiModel.label)
            TextField(uiModel.placeHolder, text: $uiModel.text)
                .keyboardType(keyboardType)
        }
    }
}
