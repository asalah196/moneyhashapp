//
//  MethodComponent.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/30/23.
//

import Foundation
import SwiftUI
import MoneyHashTask
import Kingfisher

struct MethodComponent: UIComponent {
    var uniqueId: String = UUID().uuidString
    let uiModel: MethodModel
    func render(with tapAction: (() -> ())?) -> AnyView {
        MethodView(uiModel: uiModel, tapAction: tapAction).toAny()
    }
}

struct MethodModel {
    let id = UUID()
    let name: String
    let label: String
    let icons: [String]
    let isSelected: Bool
    var imageURLs: [URL] {
        let pngIcons = icons.map { icon in
            icon.replacingOccurrences(of: ".svg", with: ".jpg")
        }

        let urls = pngIcons.map { icon -> URL? in
            if let url = URL(string: icon) {
                return url
            }
            return nil
        }
        return urls.compactMap {$0}
    }
}

struct MethodView: View {
    let uiModel: MethodModel
    var tapAction: (() -> ())?
    var body: some View {
        HStack {
            RadioButtonView(isSelected: uiModel.isSelected, borderColor: Configuration.themeColor)
                .frame(alignment: .leading)
                .padding(.leading, 10)

            Text("\(uiModel.label)")
                .lineLimit(1)
                .padding()
                .frame(maxWidth: .infinity, alignment: .leading)
            HStack {
                ForEach(uiModel.imageURLs, id: \.self) { url in
                    KFImage(url)
                }
            }
            .padding(.trailing, 10)
            .frame(maxWidth: .leastNonzeroMagnitude, alignment: .trailing)
        }
        .contentShape(Rectangle())
        .onTapGesture {
            tapAction?()
        }
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(uiModel.isSelected ? Configuration.themeColor : .gray, lineWidth: uiModel.isSelected ? 2 : 0.5)
        )
    }
}
