//
//  LabelComponent.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/29/23.
//

import Foundation
import SwiftUI

struct LabelComponent: UIComponent {
    var uniqueId: String = UUID().uuidString
    let uiModel: LabelModel
    func render(with tapAction: (() -> ())?) -> AnyView {
        LabelView(uiModel: uiModel).toAny()
    }
}

struct LabelModel {
    let alignment: Alignment
    let text: String
}

struct LabelView: View {
    let uiModel: LabelModel
    var body: some View {
        Text(uiModel.text)
            .frame(maxWidth: .infinity, alignment: uiModel.alignment)
    }
}
