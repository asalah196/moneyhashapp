//
//  FormBuilder.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/29/23.
//

import Foundation
import MoneyHashTask

struct FormBuilder {
    func buildForm(from fields: [Field]) -> [any UIComponent] {
        let components = fields.map { field -> (any UIComponent) in
            switch field {
            case is Field.Label:
                return LabelParser.parse(field)
            case is Field.Button:
                return ButtonParser.parse(field)
            case is Field.TextInput:
                return TextInputParser.parse(field)
            case is Field.Email:
                return EmailParser.parse(field)
            case is Field.Number:
                return NumberParser.parse(field)
            case is Field.Spacer:
                return SpacerParser.parse(field)
            case is Field.Method:
                return MethodParser.parse(field)
            default:
                fatalError("Unexpected type \(Field.self)")
            }
        }
        return components
    }
}
