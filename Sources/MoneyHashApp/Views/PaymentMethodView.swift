//
//  ContentView.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/28/23.
//

import SwiftUI
import Kingfisher

struct PaymentMethodView: View {
    @StateObject private var viewModel = PaymentMethodsViewModel()
    @State private var errorMessage: String = ""
    @State private var showAlert: Bool = false
    var body: some View {
        ScrollView(.vertical) {
            VStack(spacing: 20) {
                ForEach(viewModel.components, id: \.uniqueId) { component in
                    component.render {
                        withAnimation {
                            if let message = viewModel.userDidPress(component: component) {
                                errorMessage = message
                                showAlert = true
                            } else {
                                errorMessage = ""
                                showAlert = false
                            }
                        }
                    }.alert("Error", isPresented: $showAlert, actions: {}, message: {
                        Text(errorMessage)
                    })
                    }
            }.padding(.trailing, 40)
            .padding(.leading, 40)
            .padding(.top, 100)
        }
    }
}

struct PaymentMethodView_Previews: PreviewProvider {
    static var previews: some View {
        PaymentMethodView()
    }
}
