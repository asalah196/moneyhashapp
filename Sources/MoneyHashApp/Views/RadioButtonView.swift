//
//  RadioButtonView.swift
//  MoneyHash-task
//
//  Created by Ahmed Salah on 1/30/23.
//

import Foundation
import SwiftUI

struct RadioButtonView: View {
    let isSelected: Bool
    let borderColor: Color
    let maxDiam = 18.0
    var innerDiam: CGFloat {
        maxDiam/3
    }
    var body: some View {
        ZStack {
            if isSelected {
                Circle()
                    .fill(borderColor)
                    .frame(width: maxDiam, height: maxDiam)
                Circle()
                    .fill(.white)
                    .frame(width: innerDiam, height: innerDiam)
            }
            else {
                Circle()
                    .stroke(.gray)
                    .frame(width: maxDiam, height: maxDiam)
            }
        }
    }
}
